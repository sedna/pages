var sections = document.getElementsByClassName("section");
var video = document.getElementById("video");
var play_btn = document.getElementById("play_btn");

video.addEventListener("play", (event) => {
	clearInterval(interval_id);
	video.style.opacity= "0.7";
	play_btn.innerHTML = "pause sample";
});

video.addEventListener("pause", (event) => {
	interval_id = setInterval(animate_slide, 13000);
	play_btn.innerHTML = "play sample";
	video.style.opacity = "0.05";
});

video.addEventListener("ended", (event) => {
    play_btn.innerHTML = "play sample";
    video.style.opacity = "0.05";
}); 

function play() {
	if (video.paused) {
		video.play();
	} else {
		video.pause();
	}
}

var interval_id = setInterval(animate_slide, 13000);

function set_active(index) {
	sections[index].className += " active";
	sections[index].style = "display: block;";
}

function mouse_over(elem) { 
	if (!video.paused) {
    	video.pause();
    }

	//index = Array.prototype.slice.call(elem.closest('ul').querySelectorAll("li a")).indexOf(elem);
	remove_active();
	set_active(0); //index);
	clearInterval(interval_id);
	interval_id = null;
}

function mouse_out() {
	//remove_active();
	//set_active(0);
	interval_id = setInterval(animate_slide, 13000);
}

function remove_active(){
	var index = 0;
	for (var i = 0; i < sections.length; i++) {
		if (sections[i].className.indexOf("active") > -1) {
			sections[i].className = sections[i].className.replace(" active", "");
			sections[i].style = "display: none;";
			index = i + 1;
		}
	}
	return index;
}

function animate_slide(){
	var index = remove_active();
	index = index % sections.length;
	set_active(index);
}